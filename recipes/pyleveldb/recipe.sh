#!/bin/bash
DEPS_pyleveldb=(python)
BUILD_pyleveldb=$BUILD_PATH/pyleveldb/pyleveldb-git
RECIPE_pyleveldb=$RECIPES_PATH/pyleveldb

function prebuild_pyleveldb() {
	cd $BUILD_PATH/pyleveldb
	
	# Clone all repositories
	git clone https://github.com/arnimarj/py-leveldb.git pyleveldb-git
	cd $BUILD_pyleveldb
	#git clone https://github.com/google/snappy snappy
	git clone https://github.com/google/leveldb.git leveldb
}

function shouldbuild_pyleveldb() {
	if [ -d "$BUILD_pyleveldb/py-leveldb.so" ]; then
		DO_BUILD=1 #FIXME
	fi
}

function build_pyleveldb() {
	cd $BUILD_pyleveldb

	# Copy gnustl_shared
	try cp -L $ANDROIDNDK/sources/cxx-stl/gnu-libstdc++/$TOOLCHAIN_VERSION/libs/armeabi/libgnustl_shared.so $LIBS_PATH

	push_arm
	export CFLAGS="$CFLAGS -I$ANDROIDNDK/sources/cxx-stl/gnu-libstdc++/$TOOLCHAIN_VERSION/include -I$ANDROIDNDK/sources/cxx-stl/gnu-libstdc++/$TOOLCHAIN_VERSION/libs/armeabi/include"
	export CXXFLAGS=$CFLAGS
	export LDFLAGS="$LDFLAGS -L$LIBS_PATH"
	export LDSHARED=$LIBLINK
	export PYTHONPATH=$SITEPACKAGES_PATH:$BUILDLIB_PATH

	# Build Snappy
	#build_pyleveldb_snappy
	
	# Build leveldb
	build_pyleveldb_leveldb
	
	# Build pyleveldb
	build_pyleveldb_pyleveldb
	
	pop_arm
	unset LDSHARED
}

function build_pyleveldb_snappy(){
	pushd snappy
	
	# Configure
	./autogen.sh
	./configure --enable-shared --disable-static --host=arm-linux-androideabi
	
	# Build
	try make
	
	# Copy library
	try cp -L .libs/libsnappy.so $LIBS_PATH
	
	popd
}

function build_pyleveldb_leveldb(){
	pushd leveldb
	
	export TARGET_OS=OS_ANDROID_CROSSCOMPILE
	
	# Build
	try make
	
	# Copy library
	#cp -L 
	
	unset TARGET_OS
	
	popd
}

function build_pyleveldb_leveldb(){
	# Use our setup.py
	try cp -f $RECIPE_pyleveldb/setup.py $BUILD_pyleveldb/setup.py
	
	# Build and install
	try $HOSTPYTHON setup.py install
}

function postbuild_pyleveldb() {
	true
}
