#!/bin/bash

VERSION_cffi=${VERSION_cffi:-1.1.1}
DEPS_cffi=(hostpython python setuptools libffi)
URL_cffi=https://pypi.python.org/packages/source/c/cffi/cffi-${VERSION_cffi}.tar.gz
MD5_cffi=f397363bfbf99048accb0498ffc3e72b
BUILD_cffi=$BUILD_PATH/cffi/$(get_directory $URL_cffi)
RECIPE_cffi=$RECIPES_PATH/cffi

function prebuild_cffi() {
	true
}

function shouldbuild_cffi() {
	if [ -d "$SITEPACKAGES_PATH/cffi" ]; then
		DO_BUILD=0
	fi
}

function build_cffi() {
	cd $BUILD_cffi

	push_arm
	
	export PPO=$PYTHONPATH
	export PYTHONPATH=$SITEPACKAGES_PATH:$BUILDLIB_PATH
	
	#FIXME Not idempotent
	sed -i "116iinclude_dirs.append('${BUILD_PATH}/python-install/include/python2.7')" setup.py
	sed -i "116iinclude_dirs.append('${BUILD_libffi}/arm-unknown-linux-androideabi/include')" setup.py
	sed -i "116ilibrary_dirs.append('${LIBS_PATH}')" setup.py
	
	try $HOSTPYTHON setup.py install -O2 --prefix=$BUILD_PATH/python-install
	
	export PYTHONPATH=$PPO
	pop_arm
}

function postbuild_cffi() {
	true
}
