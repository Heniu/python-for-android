# This recipe isn't fully completed but we're not going to use wx in Tribler anymore.

#!/bin/bash

VERSION_wxgtk=${VERSION_wxgtk:-3.0.2}
URL_wxgtk=http://sourceforge.net/projects/wxwindows/files/3.0.2/wxWidgets-${VERSION_wxgtk}.tar.bz2
DEPS_wxgtk=(python gtk3)
MD5_wxgtk=
BUILD_wxgtk=$BUILD_PATH/wxgtk/$(get_directory $URL_wxgtk)
RECIPE_wxgtk=$RECIPES_PATH/wxgtk

function prebuild_wxgtk() {
	true
}

function shouldbuild_wxgtk() {
	if [ -d "$SITEPACKAGES_PATH/wxgtk" ]; then
		DO_BUILD=0
	fi
}

function build_wxgtk() {
	cd $BUILD_wxgtk

	push_arm

	try ./configure --with-gtk --disable-std_string --disable-precomp-headers --disable-gui --host="arm-linux-androideabi" --target=arm-linux-androideabi --prefix=$BUILD_wxgtk/build/ #try ./
	try make
	try make install
	try ldconfig

	pop_arm
}

function postbuild_wxgtk() {
	true
}
