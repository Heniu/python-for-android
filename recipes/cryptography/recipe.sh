#!/bin/bash

VERSION_cryptography=${VERSION_cryptography:-0.9}
DEPS_cryptography=(hostpython python cffi idna pyasn1)
#URL_cryptography=https://github.com/pyca/cryptography/archive/${VERSION_cryptography}.tar.gz
#MD5_cryptography= 
BUILD_cryptography=$BUILD_PATH/cryptography/master
RECIPE_cryptography=$RECIPES_PATH/cryptography

function prebuild_cryptography() {
	cd $BUILD_PATH/cryptography
	
	# Use master, as CFFI 1.0 is not in a release yet
	if [ ! -d master ]; then
		git clone https://github.com/pyca/cryptography.git master
	fi
}

function shouldbuild_cryptography() {
	if [ -d "$SITEPACKAGES_PATH/cryptography" ]; then
		DO_BUILD=1 #FIXME
	fi
}

function build_cryptography() {
	cd $BUILD_cryptography

	push_arm
	
	export PPO=$PYTHONPATH
	export PYTHONPATH=$SITEPACKAGES_PATH:$BUILDLIB_PATH
	
	try $HOSTPYTHON setup.py install -O2
	
	export PYTHONPATH=$PPO
	pop_arm
}

function postbuild_cryptography() {
	exit 1 #FIXME
}
