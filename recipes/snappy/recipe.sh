#!/bin/bash
VERSION_snappy=${VERSION_snappy:-1.1.1}
URL_snappy=https://snappy.googlecode.com/files/snappy-${VERSION_snappy}.tar.gz
DEPS_snappy=()
MD5_snappy=8887e3b7253b22a31f5486bca3cbc1c2
BUILD_snappy=$BUILD_PATH/snappy/$(get_directory $URL_snappy)
RECIPE_snappy=$RECIPES_PATH/snappy

function prebuild_snappy() {
	true
}

function shouldbuild_snappy() {
	if [ -f "$BUILD_snappy/snappy.so" ]; then
		DO_BUILD=1 #FIXME
	fi
}

function build_snappy() {
	cd $BUILD_snappy

	push_arm
	
	# Configure to build a shared library
	./configure --disable-static --host=arm-linux-androieabi

	# Make
	make

	# Copy shared library
	#???

	pop_arm
}

function postbuild_leveldb() {
	true
}
