#!/bin/bash

# dependencies of this recipe
DEPS_Tribler=(kivy openssl m2crypto twisted sqlite3 pyasn1 apsw cherrypy netifaces libtorrent libnacl libsodium pil plyvel)
 
# default build path
BUILD_Tribler=$BUILD_PATH/Tribler/ 
 
# default recipe path
RECIPE_Tribler=$RECIPES_PATH/Tribler
 
# function called for preparing source code if needed
# (you can apply patch etc here.)
function prebuild_Tribler() {
	echo "_lsprof.so" >> "${BUILD_PATH}/whitelist.txt"
	echo "_csv.so" >> "${BUILD_PATH}/whitelist.txt"

	# Clone repo
	cd $BUILD_Tribler
        git clone --recursive https://github.com/mathewvermeer/tribler.git Tribler
}
 
# function called to build the source code
function build_Tribler() {
	cd $BUILD_Tribler/Tribler
	push_arm
	try $HOSTPYTHON setup.py install
	pop_arm
}
 
# function called after all the compile have been done
function postbuild_Tribler() {
	true
}
