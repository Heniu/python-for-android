# This recipe isn't fully completed but we're not going to use wx in Tribler anymore.

#!/bin/bash

VERSION_wxpython=3.0.2.0
DEPS_wxpython=(python)
URL_wxpython=http://sourceforge.net/projects/wxpython/files/wxPython/$VERSION_wxpython/wxPython-src-$VERSION_wxpython.tar.bz2
MD5_wxpython=
BUILD_wxpython=$BUILD_PATH/wxpython/$(get_directory $URL_wxpython)
RECIPE_wxpython=$RECIPES_PATH/wxpython

function prebuild_wxpython() {
	true
}

function shouldbuild_wxpython() {
	if [ -d "$BUILD_PATH/python-install/lib/python2.7/site-packages/wxpython" ]; then
		DO_BUILD=0
	fi
}

function build_wxpython() {
	cd $BUILD_wxpython/wxPython

	push_arm

        try sed -i "252s/.*/        configure_opts = ['--host=arm-linux-androideabi', '--disable-std_string']/" $BUILD_wxpython/build/tools/build-wxwidgets.py
	try $HOSTPYTHON build-wxpython.py --build_dir=../bld --install --prefix=$BUILD_wxpython/build --extra_setup=--02 --extra_make=--host=arm-linux-androideabi

	pop_arm
}

function postbuild_wxpython() {
	true
}
