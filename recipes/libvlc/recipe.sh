#!/bin/bash

# Download location for the source code
VERSION_libvlc=${VERSION_libvlc:-2.2.0}
URL_libvlc=ftp://ftp.videolan.org/pub/videolan/vlc/${VERSION_libvlc}/vlc-${VERSION_libvlc}.tar.xz
MD5_libvlc=

# Dependencies of this recipe
DEPS_libvlc=(python)

# Default build path
BUILD_libvlc=$BUILD_PATH/libvlc/$(get_directory $URL_libvlc)

# Default recipe path
RECIPE_libvlc=$RECIPES_PATH/libvlc

# Prepares source code if needed
# (you can apply patch etc here.)
function prebuild_libvlc() {
	true
}

# Checks whether libvlc has already been built
function shouldbuild_libvlc() {
	if [ -d "$SITEPACKAGES_PATH/libvlc" ]; then
		DO_BUILD=1 #FIXME: TEMP
	fi
}

# Build from the source code
function build_libvlc() {
	cd $BUILD_libvlc

	push_arm
# --enable-orc --disable-qt
	# FIXME: "VLC 1.1 and later requires XCB libraries to deal with X11 displays. Do not disable XCB or you will not get any video support!" 
	try ./configure --disable-lua --disable-mad --disable-avcodec --disable-swscale --disable-a52 --disable-xcb --disable-alsa --disable-libgcrypt --build=i686-pc-linux-gnu --host=arm-linux-androideabi --prefix=$BUILD_libvlc/bld --enable-static #--enable-shared
	try make clean
	try make #alternative is try ./compile
	#try make install

	pop_arm

}

# Called after all of the compilation has been done, to reset environment variables or stuff alike
function postbuild_libvlc() {
	true
}
