#!/bin/bash

VERSION_libffi=${VERSION_libffi:-3.2.1}
DEPS_libffi=()
URL_libffi=https://github.com/atgreen/libffi/archive/v${VERSION_libffi}.tar.gz
MD5_libffi=9066486bcab807f7ddaaf2596348c1db
BUILD_libffi=$BUILD_PATH/libffi/$(get_directory $URL_libffi)
RECIPE_libffi=$RECIPES_PATH/libffi

function prebuild_libffi() {
	true
}

function shouldbuild_libffi() {
	if [ -e "$LIBS_PATH/libffi.so" ]; then
		DO_BUILD=0
	fi
}

function build_libffi() {
	cd $BUILD_libffi

	push_arm

	# libffi build fails if makeinfo isn't present https://github.com/atgreen/libffi/issues/111
	sed -i 's/^info_TEXINFOS = .*$/info_TEXINFOS = /' Makefile.am
	
	try ./autogen.sh
	try ./configure --host="arm-linux-androideabi" --enable-shared
	try make
	
	try cp -L $BUILD_libffi/arm-unknown-linux-androideabi/.libs/libffi.so $LIBS_PATH

	pop_arm
}

function postbuild_libffi() {
	true
}
