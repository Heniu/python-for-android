#!/bin/bash

VERSION_pycparser=${VERSION_pycparser:-2.14}
URL_pycparser=https://pypi.python.org/packages/source/p/pycparser/pycparser-$VERSION_pycparser.tar.gz
DEPS_pycparser=(python)
MD5_pycparser=a2bc8d28c923b4fe2b2c3b4b51a4f935
BUILD_pycparser=$BUILD_PATH/pycparser/$(get_directory $URL_pycparser)
RECIPE_pycparser=$RECIPES_PATH/pycparser

function prebuild_pycparser() {
	true
}

function shouldbuild_pycparser() {
	if [ -d "$SITEPACKAGES_PATH/pycparser" ]; then
		DO_BUILD=0
	fi
}

function build_pycparser() {
	cd $BUILD_pycparser
	push_arm
	try $HOSTPYTHON setup.py install
	pop_arm
}

function postbuild_pycparser() {
	true
}
