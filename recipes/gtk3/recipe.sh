# This recipe isn't fully completed but we're not going to use wx in Tribler anymore.

#!/bin/bash

VERSION_gtk3=${VERSION_gtk3:-3.16.3}
URL_gtk3=http://ftp.gnome.org/pub/gnome/sources/gtk+/3.16/gtk+-${VERSION_gtk3}.tar.xz
DEPS_gtk3=(python)
MD5_gtk3=
BUILD_gtk3=$BUILD_PATH/gtk3/$(get_directory $URL_gtk3)
RECIPE_gtk3=$RECIPES_PATH/gtk3

function prebuild_gtk3() {
	true
}

function shouldbuild_gtk3() {
	if [ -d "$SITEPACKAGES_PATH/gtk3" ]; then
		DO_BUILD=0
	fi
}

function build_gtk3() {
	try cd $BUILD_gtk3

	push_arm

	try ./configure --prefix=$BUILD_gtk3/bld --build=i686-pc-linux-gnu --host=arm-linux-androideabi --enable-shared
	try make
	try make install
	try ldconfig

	pop_arm
}

function postbuild_gtk3() {
	true
}
